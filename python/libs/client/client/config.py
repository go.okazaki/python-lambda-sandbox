import json


class Config(object):
    def __init__(self, args: dict):
        for key, value in args.items():
            setattr(self, key, value)

    def __str__(self):
        return json.dumps(self.__dict__)
