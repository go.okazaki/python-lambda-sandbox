from client import config


def test_config():
    conf = config.Config({"a": 1, "b": "x"})
    print(conf)
    assert conf.a == 1
    assert conf.b == "x"
