AWS_ACCOUNT ?= $(shell aws sts get-caller-identity --output text --query "Account")
DOCKER_REGISTRY ?= $(AWS_ACCOUNT).dkr.ecr.ap-northeast-1.amazonaws.com
IMAGE_NAME ?= $(notdir $(CURDIR))
VERSION_TAG ?= latest
IMAGE_TAG = $(DOCKER_REGISTRY)/$(IMAGE_NAME):$(VERSION_TAG)

all: build

build:
	python3 -m venv venv --upgrade-deps;\
	. venv/bin/activate;\
	pip wheel ../libs/client --no-deps -w wheels/;\
	pip install -r requirements.txt -r requirements-dev.txt wheels/*.whl;\
	black .

test:
	. venv/bin/activate;\
	pytest -s --cov=. --cov-report=lcov:coverage/lcov.info

package:
	docker build --platform linux/amd64 -t $(IMAGE_TAG) .

install:
	aws ecr get-login-password | docker login --username AWS --password-stdin $(DOCKER_REGISTRY)
	docker push $(IMAGE_TAG)

clean:
	rm -Rf venv/ wheels/ coverage/

.PHONY: all build test clean
